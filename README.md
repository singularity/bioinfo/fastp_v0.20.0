# Package fastp version 0.20.0
https://github.com/OpenGene/fastp

A FASTQ preprocessor with full features (QC/adapters/trimming/filtering/splitting...)

Package installation using Miniconda3 V4.7.12 All packages are in /opt/miniconda/bin & are in PATH fastp Version: 0.20.0
Singularity container based on the recipe: Singularity.fastp_0.20.0
## Local build:
```bash
sudo singularity build fastp_v0.20.0.sif Singularity.fastp_0.20.0
```
## Get image help:

```bash
singularity run-help fastp_v0.20.0.sif
```
### Default runscript: fastp
## Usage:
```bash
./fastp_v0.20.0.sif --help
```
or:
```bash
singularity exec fastp_v0.20.0.sif fastp --help
```
image singularity (V>=3.3) is automacly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml
can be pull (singularity version >=3.3) with:


```bash
singularity pull fastp_v0.20.0.sif oras://registry.forgemia.inra.fr/singularity/bioinfo/fastp_v0.20.0/fastp_v0.20.0:latest
```
